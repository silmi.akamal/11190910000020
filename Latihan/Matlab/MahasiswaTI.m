xi=[1.557 1.353 1.494 1.398 1.508 1.297 1.438];
yi=[1.353 1.494 1.398 1.508 1.297 1.438 1.287];

yp=[1.322 1.445 1.360 1.418 1.352 1.479 1.394];
N=7;

Xi2=xi*xi';
XiYi=xi*yi';
Sx=sum(xi);
Sy=sum(yi);


b=(N*XiYi-Sx*Sy)/(N*Xi2-Sx^2);
a=(mean(yi))-(b*mean(xi))';
xx=1.287;
yy=a+b*xx;



disp(sprintf('Sum X =%10.3f',Sx))
disp(sprintf('Sum Y =%10.3f',Sy))
disp(sprintf('X^2  =%10.6f',Xi2))
disp(sprintf('X*Y  =%10.6f',XiYi))

disp(sprintf('b  =%10.7f',b))
disp(sprintf('a  =%10.6f',a))
disp(sprintf('y  =%10.4f',yy))



plot(xi,yi,'o',xx,yy)
axis([0 25 0 15])
title('data pengamatan')
xlabel('x')
ylabel('y')
