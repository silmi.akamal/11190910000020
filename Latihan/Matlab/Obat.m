xi=[24 24 15 15 24]; %x data pesanan
yi=[20 19 15 10 20]; %y data penjualan 

Xi2=xi*xi';
YiXi=yi*xi';
Sx=sum(xi);
Sy=sum(yi);

N=5; %jumlah data
xx=24; 

b=(N*YiXi-Sx*Sy)/(N*Xi2-Sx^2); % menghitung gradien
a=(mean(yi))-(b*mean(xi)); % menghitung konstanta
yy=a+b*xx; %persamaan regresi linear

disp(sprintf('Sum X =%10.3f',Sx))
disp(sprintf('Sum Y =%10.3f',Sy))
disp(sprintf('X^2  =%10.6f',Xi2))
disp(sprintf('X*Y  =%10.6f',YiXi))

disp(sprintf('b  =%10.6f',b))
disp(sprintf('a  =%10.6f',a))
disp(sprintf('y = %10.6f',yy))

plot(xi,yi,'o',xx,yy)
axis([0 25 0 15])
title('data pengamatan')
xlabel('x')
ylabel('y')
