
xi=[1 2 3 4 5];
yi=[1034.20 1312.90 1303.22 1486.09 1481.9];
yp=[1110.214 1216.983 1323.752 1430.521 1537.29 6618.76];


Xi2=xi*xi';
XiYi=xi*yi';
Sx=sum(xi);
Sy=sum(yi);
Syp=sum(yp);
N=5;
xx=6;

b=(N*XiYi-Sx*Sy)/(N*Xi2-Sx^2)';
a=(Xi2*Sy-Sx*XiYi)/(N*Xi2-Sx^2);

yy=a+b*xx';

MAPE= mean(abs((yi-yp))./yi)*100;


disp(sprintf('Sum X =%10.6f',Sx))
disp(sprintf('Sum Y =%10.6f',Sy))
disp(sprintf('X^2  =%10.6f',Xi2))
disp(sprintf('X*Y  =%10.6f',XiYi))

disp(sprintf('b  =%10.6f',b))
disp(sprintf('a  =%10.6f',a))
disp(sprintf('y  =%10.6f',yy))
MAPE


plot(xi,yi,'o',xx,yy)
axis([0 30 0 50])
title('data pengamatan')
xlabel('x')
ylabel('y')
