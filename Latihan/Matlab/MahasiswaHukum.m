xi=[789 925 1113 980 1203 1026 1165];
yi=[925 1113 980 1203 1026 1165 1067];
yp=[1053 1061 1073 1065 1079 1068 1076];
N=7;

Xi2=xi*xi';
XiYi=xi*yi';
Sx=sum(xi);
Sy=sum(yi);
b=(N*XiYi-Sx*Sy)/(N*Xi2-Sx^2);
a=(Xi2*Sy-Sx*XiYi)/(N*Xi2-Sx^2);

xx=1067;
yy=a+b*xx;
MAPE= mean((abs(yi-yp))./yi)*100;

disp(sprintf('Sum X =%10.0f',Sx))
disp(sprintf('Sum Y =%10.0f',Sy))
disp(sprintf('X^2  =%10.0f',Xi2))
disp(sprintf('X*Y  =%10.0f',XiYi))

disp(sprintf('b  =%10.7f',b))
disp(sprintf('a  =%10.3f',a))
disp(sprintf('y  =%10.0f',yy))
disp(sprintf('MAPE  =%10.2f',MAPE))

plot(xi,yi,'o',xx,yy)
axis([0 25 0 15])
title('data pengamatan')
xlabel('x')
ylabel('y')

